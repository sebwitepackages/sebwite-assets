<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Assets\Assetic;

use Sebwite\Assets\Builder\Group;
use Sebwite\Assets\Compiler\CompiledCollection;
use Sebwite\Contracts\Assets\Factory as FactoryContract;

/**
 * This is the class AssetCollection.
 *
 * @package        Sebwite\Themes
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class AssetCollection extends \Assetic\Asset\AssetCollection implements AssetInterface
{
    /**
     * @var \Sebwite\Contracts\Assets\Factory|\Sebwite\Assets\Factory
     */
    protected $factory;

    /**
     * @var CompiledCollection
     */
    protected $compiled;

    /**
     * The Group instance
     *
     * @var Group
     */
    protected $group;

    /**
     * AssetCollection constructor.
     *
     * @param \Sebwite\Contracts\Assets\Factory|\Sebwite\Assets\Factory $factory
     * @param array                                                     $assets
     */
    public function __construct(FactoryContract $factory, array $assets)
    {
        $this->factory = $factory;

        parent::__construct($assets);
    }

    /**
     * compile method
     *
     * @return \Sebwite\Assets\Compiler\Compiled|\Sebwite\Assets\Compiler\CompiledCollection
     */
    public function compile()
    {
        if (!isset($this->compiled)) {
            $this->compiled = $this->factory->getCompiler()->compile($this);
        }

        return $this->compiled;
    }

    /** Instantiates the class */
    public function getCacheKey()
    {
        $key = '';
        foreach ($this->all() as $asset) {
            if (!$asset instanceof Asset) {
                continue;
            }
            $key .= $asset->getCacheKey();
        }

        return 'col_' . $key;
    }

    /**
     * getHandle method
     *
     * @return string
     */
    public function getHandle()
    {
        return 'col_';
    }

    /**
     * getLastModifiedHash method
     *
     * @return string
     */
    public function getLastModifiedHash()
    {
        return md5($this->getLastModified());
    }

    /**
     * Get the value of ext
     *
     * @return mixed
     */
    public function getExt()
    {
        return head($this->all())->getExt();
        #return $this->getType() === 'style' ? 'css' : 'js';
    }

    /**
     * getType method
     *
     * @return int|string
     */
    public function getType()
    {
        return $this->factory->resolveType($this);
    }

    /**
     * @return null|Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set the group value
     *
     * @param null $group
     *
     * @return AssetInterface
     */
    public function setGroup(Group $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * inGroup method
     *
     * @return bool
     */
    public function inGroup()
    {
        return isset($this->group);
    }
}
