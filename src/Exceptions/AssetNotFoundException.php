<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Assets\Exceptions;

use Exception;

/**
 * This is the class AssetNotFoundException.
 *
 * @package        Sebwite\Assets
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class AssetNotFoundException extends \Exception
{
    public function __construct($key)
    {
        $message = "Could not find asset [{$key}]";
        parent::__construct($message);
    }
}
