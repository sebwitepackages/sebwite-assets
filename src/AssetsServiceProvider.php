<?php

namespace Sebwite\Assets;

use Illuminate\Contracts\Foundation\Application;
use Sebwite\Support\ServiceProvider;

/**
 * The main service provider
 *
 * @author        Sebwite
 * @copyright     Copyright (c) 2015, Sebwite
 * @license       https://tldrlegal.com/license/mit-license MIT
 * @package       Sebwite\Assets
 */
class AssetsServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'sebwite.assets' ];

    protected $bindings = [
        'sebwite.assets.asset'         => Assetic\Asset::class,
        'sebwite.assets.collection'    => Assetic\AssetCollection::class,
        'sebwite.assets.builder.area'  => Builder\Area::class,
        'sebwite.assets.builder.group' => Builder\Group::class,
        'sebwite.assets.compiler'      => Compiler\Compiler::class,
    ];

    protected $singletons = [
        # 'sebwite.assets.finder' => AssetFinder::class
    ];

    protected $aliases = [
        'sebwite.assets'        => \Sebwite\Contracts\Assets\Factory::class,
        'sebwite.assets.finder' => AssetFinder::class,
    ];

    public function register()
    {

        $app = parent::register();
        $this->registerFinder();
        $this->registerFactory();
    }

    protected function registerFactory()
    {
        $this->app->singleton('sebwite.assets', function (Application $app) {
            $config = $app[ 'config' ]->get('sebwite.assets');

            /** @noinspection PhpParamsInspection */
            $factory = new Factory($app, $app[ 'files' ], $app[ 'url' ], $app[ 'sebwite.assets.finder' ]);
            $factory->setCachePath($config[ 'cache_path' ]);
            $factory->setDebug($config[ 'debug' ]);
            $factory->setTypes($config[ 'types' ]);

            foreach ( $config[ 'filters' ] as $extension => $filters ) {
                foreach ( $filters as $filter ) {
                    $factory->addGlobalFilter($extension, $filter);
                }
            }

            return $factory;
        });
    }

    protected function registerFinder()
    {
        $this->app->singleton('sebwite.assets.finder', function (Application $app) {
            /** @noinspection PhpParamsInspection */
            $finder = new AssetFinder($app[ 'files' ]);
            $paths  = $app[ 'config' ][ 'sebwite.assets.asset_paths' ];
            $finder->setAssetPaths($paths);

            return $finder;
        });
    }
}
