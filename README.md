Sebwite Assets
====================

[![Build Status](https://img.shields.io/travis/sebwite/assets.svg?&style=flat-square)](https://travis-ci.org/sebwite/assets)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/assets.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/assets)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/assets.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/assets)
[![Source](http://img.shields.io/badge/source-sebwite/assets-blue.svg?style=flat-square)](https://github.com/sebwite/assets)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

Sebwite Assets is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Quick Overview
-------------
For the **full documenation**, check out the [sebwite-git](/sebwite-git) package documenatation.

The `sebwite/asset` package truly gives you so much functionality and options, a quick overview simply can't even cover the basics.
So check out the full documentation to get the right impression.
