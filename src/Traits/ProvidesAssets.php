<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Assets\Traits;

/**
 * This is the class ProvidesThemeAssets.
 *
 * @package        Sebwite\Themes
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @mixin \Sebwite\Support\ServiceProvider
 */
trait ProvidesAssets
{
    /**
     * @var \Sebwite\Assets\Factory
     */
    protected $assets;

    public function enableAssets()
    {
        if (config('sebwite.themes.assets.use') !== 'sebwite') {
            return;
        }
        $registered = false;
        $this->app->make('events')->listen('composing:*', function () use (&$registered) {
            if ($registered === false) {
                $this->app->register(\Sebwite\Assets\AssetsServiceProvider::class);
                $this->assets();
                $registered = true;
            }
        });
    }

    abstract protected function assets();

    protected function area($name)
    {
        return $this->app->make('sebwite.assets')->area($name);
    }
}
