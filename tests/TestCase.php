<?php

namespace Sebwite\Tests\Assets;

abstract class TestCase extends \Sebwite\Testing\Native\AbstractTestCase
{
   /**
    * {@inheritdoc}
    */
    protected function getPackageRootPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '..';
    }
}
